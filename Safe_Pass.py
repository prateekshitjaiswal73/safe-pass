"""

    Made By - Prateekshit Jaiswal

"""

# import os

import random

import tkinter as tk

WINDOW_WIDTH = 700

WINDOW_HEIGHT = 700

WINDOW_TITLE = "Safe Pass"


class PasswordManager:
    """

    Class to create an app that saves and randomly generates password

    """

    # def __init__(self):
    #     self.c_entry = None

    def __init__(self):
        self.window = tk.Tk(

        )

        self.window.geometry(
            "{}x{}".format(WINDOW_WIDTH, WINDOW_HEIGHT)
        )

        self.window.resizable(
            width=False, height=False
        )

        self.window.configure(
            bg="#009933"
        )

        self.window.title(
            WINDOW_TITLE
        )

        self.img_label = tk.Label(
            self.window, text="Safe Pass", background="lightblue", width=30
        )

        self.img_label.place(
            x=200, y=10
        )

        self.greeting_label = tk.Label(
            self.window, text="Hello Sir/Madam Welcome to the Password Manager", width="50"
        )

        self.greeting_label.place(
            x=100, y=80
        )

        self.question_label = tk.Label(
            self.window, text="Do you want to generate a password or retriev an old password ? ", width="50"
        )

        self.question_label.place(
            x=100, y=130
        )

        self.generate_button = tk.Button(
            self.window, text="Generate",

            command=lambda: [self.hide_me(self.generate_button), self.__generate()]
        )

        self.generate_button.place(
            x=255, y=600
        )

        return
    def hide_me(self, event):
        event.place_forget()

        # self.retrieve_button = tk.Button(
        #     self.window, text="Retrieve",
        #
        #     command=lambda: [self.__retrieve()]
        # )
        #
        # self.retrieve_button.place(
        #     x=225, y=700
        # )

    def __generate(self):
        # global a


        self.statement_label = tk.Label(
            self.window, text="Password can be generated from :-", width="50"
        )

        self.statement_label.place(
            x=100, y=250
        )

        self.statement1_label = tk.Label(
            self.window, text="1. Lower =  {lower}", width="20"
        )

        self.statement1_label.place(
            x=30, y=300
        )

        self.statement2_label = tk.Label(
            self.window, text="2. Upper = {upper}", width="20"
        )

        self.statement2_label.place(
            x=30, y=350
        )

        self.statement3_label = tk.Label(
            self.window, text="3. Numbers = {numbers}", width="20"
        )

        self.statement3_label.place(
            x=30, y=400
        )

        self.statement4_label = tk.Label(
            self.window, text="4. Symbols = {symbols}", width="20"
        )

        self.statement4_label.place(
            x=30, y=450
        )

        self.choice = tk.Label(
            self.window, text="what combinations do you want ? ( just type the serial number ) ", width="50"
        )

        self.choice.place(
            x=30, y=500
        )

        # self.choice_entry = tk.Entry(
        #        master=self.window, width=40,
        #        )

        # self.choice_entry.place(
        #        x=50, y=200
        #        )
        choices_var = tk.StringVar()
        self.choices = tk.Entry(self.window,
                               textvariable=choices_var)
        choices_var = self.choices.get()

        self.choices.place(
            x=500, y=500
        )

        self.start_button = tk.Button(
            self.window, text="Start",

            command=lambda: [self.hide_me(self.start_button), self.__pass(choices_var)]
        )

        self.start_button.place(
            x=255, y=600
        )

    def __pass(self, choices_var):

        l = [self.statement_label, self.statement1_label, self.statement2_label, self.statement3_label, self.statement4_label, self.choice, self.choices]
        
        for i in l:
            i.destroy()

        lower = "abcdefghijklmnopqrstuvwxyz"

        upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        numbers = "1234567890"

        symbols = """~`!@#$%^&*()_+-={}[]:"|<>?;',./"""
        
        prob = [[],['1'], ['2'], ['3'], ['4'], ['1', '2'], ['1', '3'], ['1', '4'], ['2', '3'], ['2', '4'], ['3', '4'],
                ['1', '2', '3', ], ['1', '2', '4'], ['2', '3', '4'], ['1', '3', '4'], ['1', '2', '3', '4']]

        # c = list(input("Enter --> "))
        # print(prob.index(c))

        ans = True

        choices_var = list(choices_var)

        while ans:
            if prob.index(choices_var) == 0:

                a = lower

                break

            elif prob.index(choices_var) == 1:

                a = upper

                break

            elif prob.index(choices_var) == 2:

                a = numbers

                break

            elif prob.index(choices_var) == 3:

                a = symbols

                break

            elif prob.index(choices_var) == 4:

                a = lower + upper

                break

            elif prob.index(choices_var) == 5:

                a = lower + numbers

                break

            elif prob.index(choices_var) == 6:

                a = lower + symbols

                break

            elif prob.index(choices_var) == 7:

                a = upper + numbers

                break

            elif prob.index(choices_var) == 8:

                a = upper + symbols

                break

            elif prob.index(choices_var) == 9:

                a = numbers + symbols

                break

            elif prob.index(choices_var) == 10:

                a = lower + upper + numbers

                break

            elif prob.index(choices_var) == 11:

                a = lower + upper + symbols

                break

            elif prob.index(choices_var) == 12:

                a = upper + numbers + symbols

                break

            elif prob.index(choices_var) == 13:

                a = lower + numbers + symbols

                break

            elif prob.index(choices_var) == 14:

                a = lower + upper + numbers + symbols

                break

            else:

                self.statement_labelE = tk.Label(
                    self.window, text="Enter a valid choice like :- 1 or 12 ... \n", width="30"
                )

                self.statement_labelE.place(
                    x=220, y=400
                )

                continue
            # a = lower + upper + numbers + symbols

        self.statement_labelL = tk.Label(
            self.window, text="Enter the length of the password here --> ", width="30"
        )

        self.statement_labelL.place(
           x=220, y=400
        )

        # self.statement_labelL_entry = tk.Entry(
        #    master=self.window, width=40
        # )
        # self.statement_labelL_entry.place(
        #    x=350, y=580
        # )

        lengths_var = tk.StringVar()
        self.lengths = tk.Entry(self.window,
                               textvariable=lengths_var)
        lengths_var = self.lengths.get()

        self.lengths.place(
            x=500, y=500
        )

        # self.lengths = tk.Text(self.window,
        #                        height=5,
        #                        width=20)
        


        # length = self.lengths.get(1.0, "end-1c")

        # int(length)
        
        l = len(lengths_var)

        password = "".join(random.sample(a, l))

        return password

    def run_app(self):
        self.window.mainloop(

        )

        return


if __name__ == "__main__":
    PasswordManager(

    ).run_app(

    )
